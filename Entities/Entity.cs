﻿using System;
using System.Collections.Generic;
using System.Text;
//
using System.ComponentModel.DataAnnotations;

namespace Entities
{
    public class Entity
    {
        [Key]
        public int Id { get; set; }
        public string CreateDate { get; set; }
    }
}
