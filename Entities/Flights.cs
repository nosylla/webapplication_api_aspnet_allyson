﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Flights : Entity
    {
        public int Cupos { get; set; }
        public string Destino { get; set; }
        public string Origen { get; set; }
        public Double Precio { get; set; }
    }
}
