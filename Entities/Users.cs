﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Users: Entity
    {
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public int Cedula { get; set; }
        public string Direccion { get; set; }
        public string Telefono { get; set; }
    }
}
