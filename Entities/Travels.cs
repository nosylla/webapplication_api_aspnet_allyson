﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Travels: Entity
    {
        public int UsersId { get; set; }
        public int FlightsId { get; set; }
        public int PurchasedSeats { get; set; }
        public Users? Users { get; set; }
        public Flights? Flights { get; set; }
    }
}
