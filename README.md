Este proyecto es una pequeña web y un api para una agencia de viajes:
* Permite un CRUD de usuarios.
* Permite un CRUD de viajes.
* Permite un CRUD de boletos.


# Documentacion adicional #

En la carpeta "Doc", tambien se encuentran las instrucciones y un script de la base de datos usada.


# Tecnologías utilizadas #
* .Net Core
* Entity Framework
* Sql Server
* SWAGGER



# Paso 1: Clonar Proyecto #

Puede clonar el repositorio desde el [siguiente link](https://gitlab.com/nosylla/webapplication_api_aspnet_allyson.git)
ó
usando el siguiente comando en la terminal de Git Bash: `git clone https://gitlab.com/nosylla/webapplication_api_aspnet_allyson.git`
·

# Paso 2: Ejecutar el proyecto #

Una vez clonado el proyecto, abrir la solución la cual se encuentra en la carpeta “WebApplication” con Visual Studio 2019.


# Paso 3: Publicar los Proyectos #

Ir al “Solution Explorer”:
*	Seleccionar el proyecto “WebApplication”, luego seleccionar “Build/Publish/Folder” y establecer la ruta para el publicar el proyecto.
*	Seleccionar el proyecto “API”, luego seleccionar “Build/Publish/Folder” y establecer la ruta para el publicar el proyecto.


# Paso 4: Publicar los Proyectos #

Ir a la ruta donde se encuentra el “Publish” de cada proyecto:
*	Para arrancar el “WebApplication” y ejecutar “WebApplication.exe”. 
*	Para arrancar la “Api” y ejecutar  “API.exe”

Una vez los 2 servicios estén activos se puede navegar a las siguientes páginas:
*	WebApplication” http://localhost:5002/
*	“API” https://localhost:5001/swagger/index.html



# Descripcion #

Para el desarrollo de esta api se plantearon los siguientes casos de uso.

Una agencia de viajes desea automatizar toda la gestión de los clientes que acuden a su oficina y los viajes que estos realizan. 
La agencia proporciona la siguientes requerimientos e información de interés:

*	Se desea guardar la siguiente información de los viajeros: 
	- cédula, nombre, dirección y teléfono. 
*	Se desea agregar información de viajes disponibles para ofrecer a los clientes:
	- código de viaje, número de plazas, destino , lugar de origen y precio.
*	Un viajero puede realizar tantos viajes como desee.
