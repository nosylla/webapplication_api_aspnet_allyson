﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

$(document).ready(function () { $("input").attr("autocomplete", "off"); });



//inicio Validaciones
$('.numeric').on('input', function () {
    this.value = this.value.replace(/[^0-9]/g, '');
});

$('.precio').on('input', function () {
    this.value = this.value.replace(/[^0-9,]/g, '');
});


$('.letter').on('input', function () {
    this.value = this.value.replace(/[^a-záéíóúñA-ZÁÉÍÓÚÑ ]/g, '');
});

function validationFiels()
{
    Validar = $(".validate");
    $("form").submit(function (e) {
        var x = false;
        Validar.each(function () {
            if ($(this).val() == "" || $(this).val() == null) {
                x = true;
                $(this).css("border", "1px solid red");
            } else {
                $(this).css("border", "1px solid lightgreen");
            }
        });
        if (x) {
            e.preventDefault();
        }
    });
}
//fin Validaciones