﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//
using System.Reflection;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Entities;

namespace WebApplication.Controllers
{
    public class UsersController : Controller
    {
        private readonly IConfiguration _configuration; 
        public UsersController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: UsersController
        public async Task<IActionResult> Index()
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var item = new HttpClient())
            {
                item.BaseAddress = new Uri(url);
                HttpResponseMessage res = await item.GetAsync("api/Users");
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    ViewData["list"] = JsonConvert.DeserializeObject<List<Users>>(result);
                }
                else
                {
                    ViewData["list"] = new List<Users>();
                    ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                }
            }
            return View();
        }

        // GET: UsersController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var item = new HttpClient())
            {
                item.BaseAddress = new Uri(url);
                HttpResponseMessage res = await item.GetAsync("api/Users/" + id);
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    ViewData["item"] = JsonConvert.DeserializeObject<Users>(result);
                }
                else
                {
                    ViewData["item"] = new Users();
                    ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                }
            }
            return View();
        }

        // GET: UsersController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UsersController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Users collection)
        {
            try
            {
                string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    var item = JsonConvert.SerializeObject(collection);

                    var content = new StringContent(item, Encoding.UTF8, "application/json");
                    HttpResponseMessage res = await client.PostAsync("api/Users", content);
                    if (res.IsSuccessStatusCode)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                        return View();
                    }
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: UsersController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var item = new HttpClient())
            {
                item.BaseAddress = new Uri(url);
                HttpResponseMessage res = await item.GetAsync("api/Users/" + id);
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    ViewData["item"] = JsonConvert.DeserializeObject<Users>(result);
                }
                else
                {
                    ViewData["item"] = new Users();
                    ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                }
            }
            return View(ViewData["item"]);
        }

        // POST: UsersController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Users collection)
        {
            try
            {
                string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    var item = JsonConvert.SerializeObject(collection);

                    var content = new StringContent(item, Encoding.UTF8, "application/json");
                    HttpResponseMessage res = await client.PutAsync("api/Users/" + id, content);
                    if (res.IsSuccessStatusCode)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                        return View();
                    }
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: UsersController/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            //return RedirectToAction("Delete", "Users");
            try
            {
                string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
                using (var item = new HttpClient())
                {
                    item.BaseAddress = new Uri(url);
                    HttpResponseMessage res = await item.DeleteAsync("api/Users/" + id);
                    return RedirectToAction(nameof(Index));
                }
            }
            catch
            {
                return RedirectToAction(nameof(Index));
            }
        }

        // GET: UsersController/GetUsersByCedula/5
        public async Task<Users> GetUsersByCedula(int cedula)
        {
            Users user = null;

            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var item = new HttpClient())
            {
                item.BaseAddress = new Uri(url);
                HttpResponseMessage res = await item.GetAsync("api/Users/GetUsersByCedula/" + cedula);
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    user = JsonConvert.DeserializeObject<Users>(result);
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                }
            }
            return user;
        }
    }
}
