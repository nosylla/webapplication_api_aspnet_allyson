﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//
using System.Reflection;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Entities;

namespace WebApplication.Controllers
{
    public class FlightsController : Controller
    {
        private readonly IConfiguration _configuration;
        public FlightsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: FlightsController
        public async Task<IActionResult> Index()
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var item = new HttpClient())
            {
                item.BaseAddress = new Uri(url);
                HttpResponseMessage res = await item.GetAsync("api/Flights");
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    ViewData["list"] = JsonConvert.DeserializeObject<List<Flights>>(result);
                }
                else
                {
                    ViewData["list"] = new List<Flights>();
                    ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                }
            }
            return View();
        }

        // GET: FlightsController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var item = new HttpClient())
            {
                item.BaseAddress = new Uri(url);
                HttpResponseMessage res = await item.GetAsync("api/Flights/" + id);
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    ViewData["item"] = JsonConvert.DeserializeObject<Flights>(result);
                }
                else
                {
                    ViewData["item"] = new Flights();
                    ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                }
            }
            return View();
        }

        // GET: FlightsController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: FlightsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Flights collection)
        {
            try
            {
                string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    var item = JsonConvert.SerializeObject(collection);

                    var content = new StringContent(item, Encoding.UTF8, "application/json");
                    HttpResponseMessage res = await client.PostAsync("api/Flights", content);
                    if (res.IsSuccessStatusCode)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                        return View();
                    }
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: FlightsController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var item = new HttpClient())
            {
                item.BaseAddress = new Uri(url);
                HttpResponseMessage res = await item.GetAsync("api/Flights/" + id);
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    ViewData["item"] = JsonConvert.DeserializeObject<Flights>(result);
                }
                else
                {
                    ViewData["item"] = new Flights();
                    ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                }
            }
            return View(ViewData["item"]);
        }

        // POST: FlightsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Flights collection)
        {
            try
            {
                string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    var item = JsonConvert.SerializeObject(collection);

                    var content = new StringContent(item, Encoding.UTF8, "application/json");
                    HttpResponseMessage res = await client.PutAsync("api/Flights/" + id, content);
                    if (res.IsSuccessStatusCode)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                        return View();
                    }
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: FlightsController/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
                using (var item = new HttpClient())
                {
                    item.BaseAddress = new Uri(url);
                    HttpResponseMessage res = await item.DeleteAsync("api/Flights/" + id);
                    return RedirectToAction(nameof(Index));
                }
            }
            catch
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
