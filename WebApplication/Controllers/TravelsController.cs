﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//
using System.Reflection;
using Microsoft.Extensions.Configuration;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;
using Entities;

namespace WebApplication.Controllers
{
    public class TravelsController : Controller
    {
        private readonly IConfiguration _configuration;
        public TravelsController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // GET: TravelsController
        public async Task<IActionResult> Index()
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                HttpResponseMessage res = await client.GetAsync("api/Travels");
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    ViewData["list"] = JsonConvert.DeserializeObject<List<Travels>>(result);
                }
                else
                {
                    ViewData["list"] = new List<Travels>();
                    ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                }
            }
            return View();
        }

        // GET: TravelsController/Details/5
        public async Task<IActionResult> Details(int id)
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                HttpResponseMessage res = await client.GetAsync("api/Travels/" + id);
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    ViewData["item"] = JsonConvert.DeserializeObject<Travels>(result);
                }
                else
                {
                    ViewData["item"] = new Travels();
                    ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                }
            }
            return View();
        }

        // GET: TravelsController/Create
        public async Task<IActionResult> Create()
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                HttpResponseMessage res = await client.GetAsync("api/Flights");
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    ViewData["listFlights"] = JsonConvert.DeserializeObject<List<Flights>>(result);
                }
                else
                {
                    ViewData["listFlights"] = new List<Flights>();
                    ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                }
            }
            return View();
        }

        // POST: TravelsController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Travels collection)
        {
            try
            {
                string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);
                    var item = JsonConvert.SerializeObject(collection);

                    //Actualizar cupos de vuelo
                    HttpResponseMessage resFlights = await client.PutAsync("api/Flights/PutCupos/" + collection.FlightsId + "/" + -collection.PurchasedSeats, null);
                    if (!resFlights.IsSuccessStatusCode)
                    {
                        ModelState.AddModelError(string.Empty, "Cupos de vuelo agotados.");
                    }

                    //Crear el boleto
                    var content = new StringContent(item, Encoding.UTF8, "application/json");
                    HttpResponseMessage resTravels = await client.PostAsync("api/Travels", content);
                    if (resTravels.IsSuccessStatusCode)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                        return View();
                    }
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: TravelsController/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(url);
                HttpResponseMessage res = await client.GetAsync("api/Travels/" + id);
                if (res.IsSuccessStatusCode)
                {
                    var result = res.Content.ReadAsStringAsync().Result;
                    ViewData["item"] = JsonConvert.DeserializeObject<Travels>(result);
                }
                else
                {
                    ViewData["item"] = new Travels();
                    ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                }



                HttpResponseMessage resFlights = await client.GetAsync("api/Flights");
                if (resFlights.IsSuccessStatusCode)
                {
                    var result = resFlights.Content.ReadAsStringAsync().Result;
                    ViewData["listFlights"] = JsonConvert.DeserializeObject<List<Flights>>(result);
                }
                else
                {
                    ViewData["listFlights"] = new List<Flights>();
                    ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                }
            }
            return View(ViewData["item"]);
        }

        // POST: TravelsController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, Travels collection)
        {
            try
            {
                string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);

                    //Get boleto
                    HttpResponseMessage resGetTravels = await client.GetAsync("api/Travels/" + id);
                    if (!resGetTravels.IsSuccessStatusCode) { return RedirectToAction(nameof(Index)); }

                    var result = resGetTravels.Content.ReadAsStringAsync().Result;
                    Travels travels = JsonConvert.DeserializeObject<Travels>(result);

                    int cupos = travels.PurchasedSeats - collection.PurchasedSeats;


                    //Actualizar cupos de vuelo
                    HttpResponseMessage resFlights = await client.PutAsync("api/Flights/PutCupos/" + travels.FlightsId + "/" + cupos, null);


                    var item = JsonConvert.SerializeObject(collection);
                    var content = new StringContent(item, Encoding.UTF8, "application/json");

                    HttpResponseMessage res = await client.PutAsync("api/Travels/" + id, content);
                    if (res.IsSuccessStatusCode)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Error en el servidor: " + this.GetType().Name + "/" + MethodBase.GetCurrentMethod().Name);
                        return View();
                    }
                }
            }
            catch
            {
                return View();
            }
        }

        // GET: TravelsController/Delete/5
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                string url = _configuration.GetSection("UrlBase").GetSection("Ruta").Value;
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(url);

                    //Get boleto
                    HttpResponseMessage resGetTravels = await client.GetAsync("api/Travels/" + id);
                    if (!resGetTravels.IsSuccessStatusCode) { return RedirectToAction(nameof(Index)); }

                    var result = resGetTravels.Content.ReadAsStringAsync().Result;
                    Travels travels = JsonConvert.DeserializeObject<Travels>(result);

                    //Actualizar cupos de vuelo
                    HttpResponseMessage resFlights = await client.PutAsync("api/Flights/PutCupos/" + travels.FlightsId + "/" + travels.PurchasedSeats, null);


                    HttpResponseMessage res = await client.DeleteAsync("api/Travels/" + id);
                    return RedirectToAction(nameof(Index));
                }
            }
            catch
            {
                return RedirectToAction(nameof(Index));
            }
        }
    }
}
