﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Entities;
using API.Data;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TravelsController : ControllerBase
    {
        private readonly DataContext _context;

        public TravelsController(DataContext context)
        {
            _context = context;
        }

        // GET: api/<TravelsController>
        [HttpGet]
        public IEnumerable<Travels> Get()
        {
            return _context.Travels.OrderBy(m => m.Id);
        }

        // GET api/<TravelsController>/5
        [HttpGet("{id}", Name = "GetTravelsById")]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                if (!Exists(id)) { return NotFound(); }

                var result = await _context.Travels.SingleOrDefaultAsync(m => m.Id == id);
                result.Users = await _context.Users.SingleOrDefaultAsync(m => m.Id == result.UsersId);
                result.Flights = await _context.Flights.SingleOrDefaultAsync(m => m.Id == result.FlightsId);

                return Ok(result);
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return NoContent();
            }
        }

        // POST api/<TravelsController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Travels item)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                item.Id = 0;
                item.CreateDate = DateTime.Now.ToString();

                _context.Travels.Add(item);
                await _context.SaveChangesAsync();

                return CreatedAtRoute("GetTravelsById", new { id = item.Id }, item);
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return NoContent();
            }
        }

        // PUT api/<TravelsController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] Travels item)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                if (!Exists(id)) { return BadRequest(); }

                item.Id = id;
                item.CreateDate = DateTime.Now.ToString();

                _context.Entry(item).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return NoContent();
            }
        }

        // DELETE api/<TravelsController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                if (!Exists(id)) { return BadRequest(); }

                var result = await _context.Travels.SingleOrDefaultAsync(m => m.Id == id);

                _context.Travels.Remove(result);
                await _context.SaveChangesAsync();

                return Ok(result);
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return NoContent();
            }
        }

        private bool Exists(int id)
        {
            return _context.Travels.Any(e => e.Id == id);
        }
    }
}
