﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Entities;
using API.Data;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly DataContext _context;

        public UsersController(DataContext context)
        {
            _context = context;
        }

        // GET: api/<UsersController>
        [HttpGet]
        public IEnumerable<Users> Get()
        {
            return _context.Users.OrderBy(m => m.Id);
        }

        // GET api/<UsersController>/5
        [HttpGet("{id}", Name = "GetUsersById")]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                if (!Exists(id)) { return NotFound(); }

                var result = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);

                return Ok(result);
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return NoContent();
            }
        }

        // POST api/<UsersController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Users item)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                item.Id = 0;
                item.CreateDate = DateTime.Now.ToString();

                _context.Users.Add(item);
                await _context.SaveChangesAsync();

                return CreatedAtRoute("GetUsersById", new { id = item.Id }, item);
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return NoContent();
            }
        }

        // PUT api/<UsersController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] Users item)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                if (!Exists(id)) { return BadRequest(); }

                item.Id = id;
                item.CreateDate = DateTime.Now.ToString();

                _context.Entry(item).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return NoContent();
            }
        }

        // DELETE api/<UsersController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                if (!Exists(id)) { return BadRequest(); }

                var result = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);

                _context.Users.Remove(result);
                await _context.SaveChangesAsync();

                return Ok(result);
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return NoContent();
            }
        }

        private bool Exists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }

        /// <summary>
        /// Busca el usuario por su numero de Cedula
        /// </summary>
        /// <param name="cedula"></param>
        /// <returns></returns>
        // GET api/<UsersController>/GetUsersByCedula/5
        [HttpGet("GetUsersByCedula/{cedula}", Name = "GetUsersByCedula")]
        public async Task<IActionResult> GetUsersByCedula([FromRoute] int cedula)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                var result = await _context.Users.SingleOrDefaultAsync(m => m.Cedula == cedula);

                return Ok(result);
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return NoContent();
            }
        }
    }
}
