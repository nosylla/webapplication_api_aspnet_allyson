﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.SqlClient;
using Entities;
using API.Data;

namespace API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FlightsController : ControllerBase
    {
        private readonly DataContext _context;

        public FlightsController(DataContext context)
        {
            _context = context;
        }

        // GET: api/<FlightsController>
        [HttpGet]
        public IEnumerable<Flights> Get()
        {
            return _context.Flights.OrderBy(m => m.Id);
        }

        // GET api/<FlightsController>/5
        [HttpGet("{id}", Name = "GetFlightsById")]
        public async Task<IActionResult> Get([FromRoute] int id)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                if (!Exists(id)) { return NotFound(); }

                var result = await _context.Flights.SingleOrDefaultAsync(m => m.Id == id);

                return Ok(result);
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return NoContent();
            }
        }

        // POST api/<FlightsController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Flights item)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                item.Id = 0;
                item.CreateDate = DateTime.Now.ToString();

                _context.Flights.Add(item);
                await _context.SaveChangesAsync();

                return CreatedAtRoute("GetFlightsById", new { id = item.Id }, item);
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return NoContent();
            }
        }

        // PUT api/<FlightsController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] Flights item)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                if (!Exists(id)) { return BadRequest(); }

                item.Id = id;
                item.CreateDate = DateTime.Now.ToString();

                _context.Entry(item).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return NoContent();
            }
        }

        // DELETE api/<FlightsController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            try
            {
                if (!Exists(id)) { return BadRequest(); }

                var result = await _context.Flights.SingleOrDefaultAsync(m => m.Id == id);

                _context.Flights.Remove(result);
                await _context.SaveChangesAsync();

                return Ok(result);
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return NoContent();
            }
        }

        private bool Exists(int id)
        {
            return _context.Flights.Any(e => e.Id == id);
        }

        /// <summary>
        /// Actualiza los cupos del vuelos
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cupos"></param>
        /// <returns></returns>
        // PUT api/<FlightsController>/PutCupos/5
        [HttpPut("PutCupos/{id}/{cupos}", Name = "PutCupos")]
        public async Task<IActionResult> PutCupos(int id, int cupos)
        {
            if (!ModelState.IsValid) { return BadRequest(ModelState); }

            Flights item = new Flights();

            try
            {
                item = await _context.Flights.SingleOrDefaultAsync(m => m.Id == id);

                if (item.Cupos == 0 && cupos < 0) { return BadRequest(); }

                item.Cupos = item.Cupos + cupos;

                _context.Entry(item).State = EntityState.Modified;
                await _context.SaveChangesAsync();

                return Ok();
            }
            catch (SqlException e)
            {
                Console.WriteLine(e.Message);
                return NoContent();
            }
        }
    }
}
